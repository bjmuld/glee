# {{ title }}

This is a paragraph of text.  It will get rendered as
plain markdown

you can include a table, for example:

| Row Number | Some numbers  | 
| ---------- | ------------  |
| 1          | 1, 2, 3, 4    |
| 2          | 5, 6, 7, 8    |
| 3          | 9             |

    ...or anything else for that matter.

```bash
$ echo "toot toot"
```
When you have a Group of questions:

#### Here is a group of questions [15 pts]:
- [+5] How many oranges fit in an apple cart?
- [+1] What color are the oranges?
- [+9] Explain how will you deal with them?

Those Questions will render as:

#### 1. Here is a group of questions [15 pts]:

1. How many oranges fit in an apple cart?

    [+5 pts] Answer:

2. What color are the oranges?

    [+1 pt] Answer:

3. Explain how you will deal with them?

    [+9 pts] Answer: