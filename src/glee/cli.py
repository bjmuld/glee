import sys
import os
import argparse
import subprocess
import string

from pathlib import Path
from dateutil.parser import parse as dateparse
from dateutil import tz


class GleeCommand(dict):
    def __init__(self, function, alias=None, aliases=[]):
        if isinstance(aliases, str):
            aliases = [aliases]
        if alias and isinstance(alias, str):
            aliases += [alias]

        super().__init__({
            'name': function.__name__,
            'callable': function,
            'aliases': [function.__name__] + aliases
        })

        self['aliases'] += [item.replace('_', '-') for item in self['aliases']]
        self['aliases'] = list(set(self['aliases']))

    def __contains__(self, item):
        return item in self['aliases']

    def __call__(self, *args, **kwargs):
        return self['callable'](*args, **kwargs)


class GleeCommandList(dict):
    def __init__(self, list_of_commands):
        super().__init__()
        for c in list_of_commands:
            self.update({c['name']: c})

    def __contains__(self, item):
        for name, cmd in self.items():
            if item in cmd:
                return True
        return False

    def __getitem__(self, item):
        for name, cmd in self.items():
            if item in cmd:
                return cmd
        return None


def init(glee, ap):
    """
    Turns a local Git repo into a Glee repo.  If no local repo exists, one will be made.
    """
    ap.add_argument('path', help='local path to the project repo', nargs='?', default='.')
    args = ap.parse_args()
    return glee.init_repo(args.path)


def schedule(glee, ap):
    return glee.process_schedule()


def syllabus(glee, ap):
    return glee.process_syllabus()


def readme_links(glee, ap):
    return glee.process_important_links()


def grades(glee, ap):
    return glee.process_gradebook()


def encrypt_gradebook(glee, ap):
    ap.add_argument('input_file', help="path to gradebook file")
    ap.add_argument('summary_sheet_name', help="name of summary sheet in gradebook")
    ap.add_argument('output_file', help="Path to output encrypted grades file.")
    args = ap.parse_args()
    return glee.encrypt_grade_markdown(
        input_file=args.input_file,
        sheet_name=args.summary_sheet_name,
        output_file=args.output_file)


def post(glee, ap):
    subprocess.run(['bash',
                    '-c',
                    'cd {} && git commit -am \"automated commit by \`glee post\`\" && git push'.format(
                        str(glee.root / glee._resolve_local_path('student-view/BULLETIN')))
                    ]
                   , check=False)


def fork_submissions(glee, ap):
    lap = argparse.ArgumentParser(parents=[ap])
    lap.add_argument('--student', '-s', default='*', help='student whose assignment to fetch', nargs=1)
    lap.add_argument('assignment_slugs', nargs='+', help='slugs of assignment to fork')
    args = lap.parse_intermixed_args()

    return glee.fork_submission(assignment_slug=args.assignment_slugs, student=args.student)


def fetch_assignment(glee, ap):
    lap = argparse.ArgumentParser(parents=[ap])
    lap.add_argument('--student','-s',
                     help='which student whose assignment to fetch', action='append')
    lap.add_argument('assignment_slugs', nargs="+", action='store', help='slug (short name) of assignment to fetch')
    # lap.add_argument('--source', '-S',
    #                  help='fetch the assignment from either: \{\'submissions\',\'forks\'\}',
    #                  action='store',
    #                  default='forks')
    args = lap.parse_intermixed_args()
    if not args.student:
        args.student = '*'
    # glee.fork_submission(assignment_slug=args.assignment_slugs, student=args.student)
    return glee.fetch_submission(assignment=args.assignment_slugs, student=args.student)


def help(glee, ap):
    print(_help_txt())


def test_grades(glee, ap):
    from .show_grades import main as show_grades_ext
    file = glee._resolve_local_path('student-view/bulletin') / 'grades.md'

    for stu in glee.student_info:
        show_grades_ext(file=file, id=stu['Student Number'], pw=stu['password'], write_csv=False)

    poss_decr_file = file.parent / (file.stem + '_decrypted.csv')
    if poss_decr_file.is_file():
        os.remove(poss_decr_file)


# def backup_assignment(glee, ap):
#     ap.add_argument('assignment_slug')
#     args = ap.parse_args()
#     return glee.backup_assignment(args.assignment_slug)


# def backup(glee, ap):
#     ap.add_argument('target', nargs='?', default='.', help='item to backup')
#     args = ap.parse_args()
#
#     if args.target == '.':
#         args.target = Path(args.target).absolute().name
#     else:
#         args.target = Path(args.target)
#
#     return glee.backup_assignment(args.target)


def post_assignment(glee, ap):
    ap.add_argument('assignment_directory')
    ap.add_argument('assignment_slug')
    ap.add_argument('due_date', nargs='*')
    args = ap.parse_args()
    return glee.post_assignment(assn_dir=args.assignment_directory,
                                assn_slug=args.assignment_slug,
                                due_date=' '.join(args.due_date))


def post_solution(glee, ap):
    ap.add_argument('assignment_directory')
    ap.add_argument('assignment_slug')
    args = ap.parse_args()
    return glee.post_solution(assn_dir=args.assignment_directory,
                              assn_slug=args.assignment_slug)


def set_due_date(glee, ap):
    ap.add_argument('assignment_slug', help='assignment slug whose due date to set')
    ap.add_argument('due_date', nargs='+', help='new due date for the assignment')
    args = ap.parse_args()
    return glee.set_due_date(args.assignment_slug, ' '.join(args.due_date))


def get_due_date(glee, ap):
    ap.add_argument('assignment_slug', help='assignment slug whose due date to get')
    args = ap.parse_args()
    print(glee.get_due_date(args.assignment_slug))


def set_slug(glee, ap):
    ap.add_argument('slug', help='set section slug for this glee repo')
    args, unknargs = ap.parse_known_args()
    return glee.set_section_slug(args.slug)


def set_token(glee, ap):
    ap.add_argument('gitlab_token', help='set instructor\'s gitlab token for use in this glee repo')
    args, unknargs = ap.parse_known_args()
    return glee.set_gitlab_token(args.gitlab_token)


def score(glee, ap):

    ap.add_argument('file', nargs='?', help='assignment file to score', default='README.md')
    args = ap.parse_args()

    assert Path(args.file).is_file(), _edie('file not found')

    file = glee.read_assignment_file(file=args.file)
    file.compute_score()
    file.write()

    print('section-scores:\n{}\n'.format(','.join(['{:g}'.format(x['score']) for x in file.sections])))
    print('total: {}\n'.format(file.metadata.get('Score')))


def sections(glee, ap):
    ap.add_argument('file', nargs='?', help='assignment file', default='README.md')
    args = ap.parse_args()

    assert Path(args.file).is_file(), _edie('file not found')

    file = glee.read_assignment_file(file=args.file)
    print(','.join([s['title'] for s in file.sections]))


def summarize_scores(glee, ap):

    ap.add_argument('assignment_dir', nargs='?', help='assignment whose scores to summarize')
    ap.add_argument('filename', nargs='?', default='README.md', help='filename to use for scoring')
    args = ap.parse_args()
    if not args.assignment_dir:
        args.assignment_dir = os.getcwd()

    targ_dir = Path(args.assignment_dir)
    assert targ_dir.is_dir(), _edie('directory not found')

    student_dirs = [Path(x) for x in os.listdir(targ_dir)]
    if not student_dirs or not all([Path(x).is_dir() for x in student_dirs]):
        _edie('no student work found in \'{}\'. Should be a directory which contains only per-student '
              'submission directories '.format(targ_dir))

    for sd in student_dirs:
        file = glee.read_assignment_file(file=sd/args.filename)
        file.compute_score()
        print('{:25s}: {}'.format(str(sd), ','.join(['{:g}'.format(x) for x in file.get_section_scores()])))
        print('{:25s}: {}'.format('    total', file.get_score()))


def make_shared_submission_groups(glee, ap):
    args = ap.parse_args()
    return glee.make_shared_submission_groups()


def make_shared_submission_repos(glee, ap):
    ap.add_argument('repo_name', help='Long name to use for repo in all shared submission groups')
    ap.add_argument('repo_path', help='Path to use for repos in all shared submission groups')
    args = ap.parse_args()
    return glee.make_shared_submission_repos(args.repo_name, args.repo_path)


def fetch_submission_repos(glee, ap):
    ap.add_argument('repo_path', help='Path of submission repo relative to \'submissions\' group.')
    args = ap.parse_args()
    return glee.fetch_student_submission_repos(args.repo_path)


def set_instructor(glee, ap):
    ap.add_argument('-t', '--gitlab_token', help='instructor\'s Gitlab auth token')
    args = ap.parse_args()

    glee.set_instructor(gitlab_token=args.gitlab_token)


def git_log(glee, ap):
    ap.add_argument('target_repo', nargs='?', help='repo whose git log to translate', default='.')
    args = ap.parse_args()

    return glee.translate_git_log(args.target_repo)


def new_assignment(glee, ap):
    ap.add_argument('assignment_name', help='name of new assignment to create')
    args = ap.parse_args()

    glee.create_assignment(name=args.assignment_name)


def default_timezone(glee, ap):
    ap.add_argument('timezone', nargs='?', help='specify the timezone')
    args = ap.parse_args()

    glee.set_default_tz(args.timezone)


def clone(glee, ap):
    ap.add_argument('source', help='source (can be git or local file-system, but must be a Glee course)')
    ap.add_argument('destination', nargs='?', help='destination for clone. use like you would use \'git clone\'')
    args = ap.parse_args()

    glee.clone_course(args.source, args.destination)


def garbage_collect(glee, ap):
    glee.collect_garbage()


GLEE_COMMANDS = GleeCommandList([
    GleeCommand(init,               aliases=[]),
    GleeCommand(schedule,           aliases=['make-schedule']),
    GleeCommand(syllabus,           aliases=['make-syllabus']),
    GleeCommand(readme_links,       aliases=['links', 'readme-links', 'readme']),
    GleeCommand(grades,             aliases=[]),
    GleeCommand(post,               aliases=['post-bulletin']),
    GleeCommand(fetch_assignment,   aliases=['fetch-assignments']),
    GleeCommand(help,               aliases=[]),
    GleeCommand(test_grades,        aliases=['test-grades']),
    # GleeCommand(backup_assignment,  aliases=[]),
    # GleeCommand(backup,             aliases=[]),
    GleeCommand(post_assignment,    aliases=['publish-assignment']),
    GleeCommand(set_due_date,       aliases=['set-duedate', 'set-due-date', 'set-deadline']),
    GleeCommand(get_due_date,       aliases=[]),
    GleeCommand(set_slug,           aliases=['section-slug']),
    GleeCommand(set_token,          aliases=[]),
    GleeCommand(score,              aliases=[]),
    GleeCommand(summarize_scores,   aliases=[]),
    GleeCommand(git_log,            aliases=['gitlog', 'git-log']),
    GleeCommand(set_instructor,     aliases=['instructor']),
    GleeCommand(new_assignment,     aliases=[]),
    GleeCommand(default_timezone,   aliases=['timezone', 'set-timezone']),
    # GleeCommand(publish_assignment, aliases=['post-assignment']),
    GleeCommand(clone,              aliases=[]),
    GleeCommand(sections,           aliases=[]),
    GleeCommand(fork_submissions,   aliases=['fork-submission']),
    GleeCommand(post_solution,      aliases=['post-solutions']),
    GleeCommand(garbage_collect,    aliases=['gc', 'collect-garbage']),
    GleeCommand(make_shared_submission_groups, aliases=['make-submission-groups']),
    GleeCommand(fetch_submission_repos, aliases=['clone-submission-repos']),
    GleeCommand(make_shared_submission_repos, aliases=['make-submission-repos']),
    GleeCommand(encrypt_gradebook, aliases=['standalone-grades']),
])


def _edie(msg='', code=1):
    print(msg, file=sys.stderr)
    sys.exit(code)


def _confirm(message='Do you want to proceed [y/N]?', default=False):
    response = input(message)
    if response in 'yY':
        return True
    else:
        return False


def _ask_for_duedate():
    dd = None
    while not dd:
        dd = input('what due-date would you like to use for the assignment?  ').strip()
        try:
            return dateparse(dd)

        except:
            dd = None


def _help_txt():
    txt = 'Glee commands:\n'
    cmd_names = sorted(GLEE_COMMANDS.keys())
    for name in cmd_names:
        cmd = GLEE_COMMANDS[name]
        aliases = ','.join([x for x in cmd['aliases'] if x != name])
        if aliases:
            txt += '   {:25s}   ({})\n'.format(name, aliases)
        else:
            txt += '   {:25s}\n'.format(name)
    return txt


def _fs_safe(unsafe_string):
    safechars = string.ascii_letters + string.digits + " -_."
    try:
        return ''.join(filter(lambda c: c in safechars, unsafe_string))
    except:
        return ""


def _main():
    from .glee import Glee

    ap = argparse.ArgumentParser(add_help=False)
    ap.add_argument('command', default=None, nargs='?', help=_help_txt())
    # ap.add_argument('--help', action='store_true')
    ap.add_argument('--debug', action='store_true')
    args, uargs = ap.parse_known_args()

    glee = Glee('.')

    if not args.command or args.command not in GLEE_COMMANDS:
        help(glee, ap)

    elif args.command in GLEE_COMMANDS:
        if glee.root is None:
            if not any(args.command in f for f in [GLEE_COMMANDS['init'],
                                                    GLEE_COMMANDS['clone'],
                                                    GLEE_COMMANDS['encrypt_gradebook']
                                    ]):
                _edie('This directory hasn\'t been set up as a Glee root. Try running \'glee init\'')

        sys.exit(GLEE_COMMANDS[args.command](glee, ap))

    else:
        _edie('Glee did not recognize that command')