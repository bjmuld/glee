#! /usr/bin/env python3

import argparse
import sys
import shutil
import subprocess
import os

import git.exc
import pandas as pd
import base64
import numpy as np
import yaml
import platform
import re
import yatter
import datetime
import jinja2
import urllib

from copy import deepcopy
from gitlab import Gitlab
from gitlab.const import DEVELOPER_ACCESS
from gitlab.v4.objects import GroupSubgroup, GroupProject, Project
from gitlab.exceptions import GitlabGetError, GitlabCreateError, GitlabDeleteError
import gitlab.const

from pathlib import Path
from git import Repo, exc, Remote

from dateutil.parser import parse as dateparse
from dateutil import tz

from cryptography.hazmat.backends import default_backend as _default_backend
from cryptography.hazmat.primitives import hashes as _hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC as _PBKDF2HMAC
from cryptography.fernet import Fernet as _Fernet

from .cli import _confirm, _edie, _fs_safe, _ask_for_duedate

COURSE_CONF_DIR = Path('.glee')
COURSE_CONF_GITLAB_TREE = COURSE_CONF_DIR / 'gitlab-tree'
COURSE_CONF_LOCAL_TREE = COURSE_CONF_DIR / 'local-tree'
COURSE_CONF_GLEE_VARS = COURSE_CONF_DIR / 'glee-vars'
COURSE_CONF_SECTION_SLUG = COURSE_CONF_DIR / 'section-slug'
GLEE_TMP = COURSE_CONF_DIR / 'tmp'

if 'linux' in platform.system().lower():
    if 'XDG_CONFIG_HOME' in os.environ:
        USER_CONF_DIR = Path(os.environ['XDG_CONFIG_HOME']) / 'glee'
    else:
        USER_CONF_DIR = Path('~/.config/glee').expanduser()

elif 'win' in platform.system().lower():
    USER_CONF_DIR = Path('~/AppData/glee').expanduser()

DEFAULT_GIT_HIERARCHY = {
    'website': {
        '_name': 'website'},
    'instructor': {
        '_name': 'instructor'},
    'reading': {
        '_name': 'reading'},

    'section': {
        '_name': '::section::',

        'instructor': {
            '_name': 'instructor',
            'submissions': {
                '_name': 'submissions'},
            'section-data': {
                '_name': 'section-data'},
        },

        'student': {
            '_name': 'student',
            'bulletin': {
                '_name': 'bulletin'},
            'assignments': {
                '_name': 'assignments'},
            'student-area': {
                '_name': 'student-area'},
        },
    }
}

DEFAULT_LOCAL_HIERARCHY = {
    '_git': '::course::/instructor',

    'assignments': {
        '_name': 'assignments'},
    'lectures': {
        '_name': 'lectures'},
    'metamaterial': {
        '_name': 'metamaterial'},

    'reading': {
        '_name': 'reading',
        '_git': '::course::/reading'},
    'section-data': {
        '_name': 'section-data',
        '_git': '::course::/::section::/instructor/section-data'},

    'student-view': {
        '_name': '_STUDENT-VIEW_',
        'bulletin': {
            '_name': 'bulletin',
            '_git': '::course::/::section::/student/bulletin'},
    },

    'grading': {
        '_name': 'grading',
        '.gitignore': {
            '_name': '.gitignore',
            '_contents': '*'},
    },
    '_NO-TRACK_': {
        '_name': '_NO-TRACK_',
        '.gitignore': {
            '_name': '.gitignore',
            '_contents': '*'},
    },
}


class GleeMetaData:
    regex = r'^\[Glee Metadata\]: # \((.*?)\)\n\n---\n\n'
    prefix = '[Glee Metadata]: # ('
    suffix = ')\n\n---\n\n'

    def __init__(self, data=None, text=None, file=None):
        if file and Path(file).is_file():
            text = Path(file).read_text()
        if text:
            self.load(text)
        if data is None:
            data = {}
        self.data = data

    def dump(self):
        return self.prefix + yaml.dump(self.data).replace('\n', '%0a') + self.suffix

    def _split(self, text):

        coretext = re.match(self.regex, text)
        if coretext:
            head_data = coretext.group(1).strip()
            head_data = head_data.replace('%0a', '\n')
            head_data = yaml.safe_load(head_data)

            return head_data, text.split(coretext.group(0))[1]
        else:
            return None, text

    def load(self, text):
        self.data = self._split(text)[0]

    def split(self, text):
        data, body = self._split(text)
        if data:
            self.data = data
        return self, text

    def assign(self, key, value):
        # if key in self.data:
        self.data[key] = value

    def delete(self, key):
        if key in self.data:
            del self.data[key]

    def get(self, key):
        if key in self.data:
            return self.data[key]


class GleeAssignmentMetaData(GleeMetaData):
    title_field = 'Title'
    date_assigned_field = 'Date-assigned'
    date_due_field = 'Date-due'
    points_avail_field = 'Points-available'
    score_field = 'Score'
    section_scores_field = 'Section-scores'

    all_fields = [
        title_field,
        date_assigned_field,
        date_due_field,
        points_avail_field,
        score_field,
        section_scores_field
    ]

    def __init__(self, data=None, text=''):
        super().__init__(data=data, text=text)
        for i in self.all_fields:
            if i not in self.data:
                self.data.update({i: None})

    def set_score(self, score):
        self.data.update({self.score_field: score})

    def get_score(self):
        return self.data[self.score_field]

    def set_section_scores(self, section_scores):
        self.data.update({self.section_scores_field: ','.join([str(x) for x in section_scores])})

    def get_section_scores(self):
        section_scores = []
        for x in self.data[self.section_scores_field].split(','):
            try:
                section_scores.extend([float(x)])
            except:
                pass
        return section_scores


class GleeAssignmentFile:
    title_regex = r'^# (.*)$'
    section_start_regex = r'^#{3,5}\s+(Section|(\d+[.]?)+)\s*(.*)$'
    # section_end_regex = r'^#{1,3} (.*)$'
    blockquote_pattern = r'^```'
    points_pattern = r'\[([+-]\d+[.]?\d*)(?:\s*points|pts|Pts|Points)?\]'

    def __init__(self, text='', file=None):

        self.file = file

        if self.file and not text:
            self.file = Path(file)
            if self.file.is_file():
                text = self.file.read_text()
            else:
                _edie('file \'{}\' not found'.format(file))
                text = ''

        if text:
            self.metadata, self.body = GleeAssignmentMetaData().split(text)
        else:
            self.metadata = GleeAssignmentMetaData()
            self.body = ''

        if self.body:

            self.sections = []
            context = ''
            sec_body = ''
            in_quote = False
            is_titled = False

            for line in self.body.splitlines():

                if in_quote:
                    if re.match(self.blockquote_pattern, line):
                        in_quote = False
                    sec_body += '\n' + line
                    continue

                if re.match(self.blockquote_pattern, line):
                    in_quote = True
                    sec_body += '\n' + line
                    continue

                title = re.match(self.title_regex, line)
                if title and not is_titled:
                    self.metadata.assign('Title', title.group(1))
                    is_titled = True
                    continue

                sec_head = re.match(self.section_start_regex, line)
                if sec_head:
                    if context and sec_body:
                        self.sections.append({'title': context, 'text': sec_body})
                        context = ''
                        sec_body = ''
                    context = sec_head.group(1)
                    sec_body = ''

                # sec_end = re.match(self.section_end_regex, line)
                # if sec_end:

                if context and not sec_head:
                    sec_body += '\n' + line
                    continue

            if not self.title:
                if file:
                    _edie('assignment file {} is missing a title'.format(file))
                else:
                    _edie('tried creating a glee assignment but markdown is missing a title (# heading)')

            # one final write:
            if context and sec_body:
                self.sections.append({'title': context, 'text': sec_body})

    @property
    def title(self):
        return self.metadata.get('Title')

    def compute_score(self, posting=False):

        total_score = 0

        if self.sections:
            for section in self.sections:
                points = []
                for line in section['text'].splitlines():
                    if line.lstrip().startswith('#'):
                        continue
                    else:
                        points += re.findall(self.points_pattern, line.replace(' ', ''))

                total = sum([float(item) for item in points])
                section['score'] = total
                total_score += total

        if not self.sections:
            points = []
            for line in self.body.splitlines():
                if line.lstrip().startswith('#'):
                    continue
                else:
                    points += re.findall(self.points_pattern, line.replace(' ', ''))

            total = sum([float(item) for item in points])
            total_score += total

        if not posting:
            self.metadata.assign('Section-scores', [s['score'] for s in self.sections])
            self.metadata.assign('Score', total_score)

        elif posting:
            scoredict = [{s['title']: s['score']} for s in self.sections]
            scoredict.append({'TOTAL': total_score})
            self.metadata.assign('Points-available', scoredict)
            self.metadata.delete('Score')
            self.metadata.delete('Section-scores')

    def write(self, file=None):
        if file is None and self.file is None:
            raise RuntimeError('must specify a file')
        with open(self.file, 'w') as f:
            f.write(self.metadata.dump())
            f.write('# ' + self.title + '\n')
            if self.metadata.get('Date-due') is not None:
                self.body = re.sub(r'^### Due \w{3,4} \w{3,4} \d{2} \d{2}:\d{2}:\d{2} \d{4} \w{3}\s*$', '', self.body, flags=re.MULTILINE)
                f.write('\n' + '### Due ' + self.metadata.get('Date-due') + '\n\n---\n\n')

            started = False
            for line in self.body.splitlines():
                if line.lstrip().startswith('# '):
                    continue
                if line.lstrip().startswith('[Glee Metadata]: # ('):
                    continue

                if not started:
                    if re.match(r'\s*?[-]+\s*?', line):
                        continue
                    elif re.match(r'\s*$', line):
                        continue
                    else:
                        started = True

                f.writelines([line])
                f.write('\n')
            if not f.seek(0, 2) == '\n':
                f.write('\n')


# class PathtreeNode(dict):
#     def __init__(self, contentsDict, fullpath=None):
#         super().__init__(contentsDict)
#         self.update({'_path': fullpath})

    # def realpath(self):
    #     parts = self['_name']
    #     parent = self['_parent']
    #     while type(parent) is PathtreeNode:
    #         parts.append(parent['_name'])
    #         parent = parent['_parent']
    #     return parts


# class Pathtree(dict):
#
#     def __init__(self, dict_of_tree, rootpath='/'):
#         super().__init__(dict_of_tree)
#         self.update({'_path': rootpath})
#
#     def set_root(self, newroot):
#         self['_path'] = str(newroot)
#
#     def get_node_by_function(self, path):
#         cur_dict = self
#         for p in Path(path).parts:
#             cur_dict = cur_dict[p]
#         return cur_dict
#
#     def get_realpath(self, path):
#         node = self.get_node_by_function(path)
#         return node['_path']

    # def insert(self, dict_of_tree, path, is_root=False):
    #
    #     path = Path(path)
    #
    #     if is_root:
    #         doi = self
    #     else:
    #         doi = self.get_node_by_function(path)
    #
    #     for k, v in dict_of_tree.items():
    #
    #         #properties
    #         if k.startswith('_'):
    #             doi.update({k: v})
    #             continue
    #
    #         if type(v) is dict:
    #             # new node:
    #             doi.update({k: PathtreeNode({}, fullpath=Path(doi['_path']) / v['_name'])})
    #             self.insert(v, path / k)


class Glee:

    def __init__(self, root_path):
        self.__safe_to_save = False
        self._load_defaults()

        gr = self.find_glee_root(root_path)
        if gr:
            self.root = Path(gr)
            self._load_instructor_vars()
            self._load_section_vars()
            self._load_gitlab_tree()
            self._load_local_tree()
            self.__safe_to_save = True
            # self.files = self._fix_relative_paths_dict(self.files)
            self._read_section_slug()
            self.student_info = self._read_student_info(
                self.root / self.local_tree['section-data']['_name'] / self.filenames['GRADEBOOK'],
            )

    def __del__(self):
        self.save()

    def _load_defaults(self):
        self.root = None
        self.gitlab = None
        self.instructor_gitlab_token = ''
        self.instructor_gitlab_username = ''
        self.instructor_email = ''
        self.instructor_name = ''
        self.section_slug = ''
        self.gitlab_root = ''
        self.default_tz = 'America/Chicago'
        self.filenames = {
            'SHOW_GRADES_SCRIPT': 'show-grades',
            'GRADES_FILE_ENCR': 'grades.md',
            'USER_CONF_FILE': 'instructor',
            'GRADEBOOK': 'gradebook.ods',
        }
        self.ods_sheet_names = {
            'gradebook': {
                'ROLL_SHEET_NAME': 'Roll',
                'GRADE_SHEET_NAME': 'Grades',
            },
        }
        self.gitlab_tree = DEFAULT_GIT_HIERARCHY
        # {
        #     'root': '::course::',
        #     'assigned': '::course::/::section::/student/assignments',
        #     'submissions': '::section::/instructor/submissions',
        #     'bulletin': 'bulletin',
        #     'reading': 'reading',
        #     'section-data': 'section-data',
        #     'instructor': 'instructor',
        #     'student-view': 'student-view',
        #     'instructor-view': 'instructor-view',
        # }

        self.local_tree = DEFAULT_LOCAL_HIERARCHY
        self._skels= {
            'assignment': Path(__file__).absolute().parent / 'collateral' / 'skel_assignment',
            'course': Path(__file__).absolute().parent / 'collateral' / 'skeleton_course',
            'show-grades-script': Path(__file__).absolute().parent / 'show_grades.py',
        }

        # self.paths = {
            # 'PUBLIC_DIR': '_STUDENT-VIEW_',
            # 'SECTION_DATA': 'section-data',
            # 'GRADING_DIR': 'grading',
            # 'BULLETIN': '_STUDENT-VIEW_/bulletin',
            # 'ASSIGNMENTS': 'assignments',
            # 'BULLETIN_SRC_DIR': 'metamaterial',
            # 'GLEE_TMP': '.glee_tmp',
            # 'SKELETON_COURSE': Path(__file__).absolute().parent / 'collateral' / 'skeleton_course'
        # }

        # self.files = {
        #     'SCHEDULE_ODS': 'metamaterial/schedule.ods',
        #     'BULLETIN_README_TEMPLATE': 'metamaterial/bulletin-readme-template.md',
        #     'IMPORTANT_LINKS': 'metamaterial/important-links.md',
        #     'SYLLABUS_TEX': 'metamaterial/syllabus.tex/syllabus.tex',
        #     'GRADEBOOK': 'section-data/gradebook.ods',
        # }

    def _collect_garbage(self):
        tmpdir = self.root / GLEE_TMP
        try:
            shutil.rmtree(tmpdir)
        except FileNotFoundError:
            pass

    def _read_section_slug(self):
        slugfile = self.root / COURSE_CONF_SECTION_SLUG
        if not slugfile.is_file():
            slug = None
            while not slug:
                slug = input('No section-slug file found. What section slug (e.g. \'21sp\') do you want to use for this section of the course?'
                             ).strip().strip(' ')
                slug = urllib.parse.quote_plus(slug)
            self.section_slug = str(slug)
            self._write_slug_file()
        else:
            self.section_slug = slugfile.read_text().strip()

    def _write_slug_file(self):
        (self.root / COURSE_CONF_SECTION_SLUG).write_text(self.section_slug)

    def _load_instructor_vars(self):
        instructor_file = USER_CONF_DIR / self.filenames['USER_CONF_FILE']
        if not instructor_file.is_file():
            print('creating empty instructor config at {}'.format(instructor_file))
            if not instructor_file.parent.is_dir():
                os.makedirs(instructor_file.parent)
            with open(instructor_file, 'w') as f:
                f.writelines(['instructor_gitlab_token: \n', 'instructor_gitlab_username: \n'])

        self._read_glee_file(instructor_file)

    def _load_section_vars(self):
        if self.root:
            section_vars_file = self.root / COURSE_CONF_GLEE_VARS
            if not section_vars_file.is_file():
                _edie('you are not within a glee section directory. Move into one or use \'glee init\' in this one.')
            self._read_glee_file(section_vars_file)

    def _load_gitlab_tree(self):
        if (self.root / COURSE_CONF_GITLAB_TREE).is_file():
            with open(self.root / COURSE_CONF_GITLAB_TREE, 'r') as f:
                tree = yaml.safe_load(f)
            self.gitlab_tree.update(tree)

    def _load_local_tree(self):
        if (self.root / COURSE_CONF_LOCAL_TREE).is_file():
            with open(self.root / COURSE_CONF_LOCAL_TREE, 'r') as f:
                tree = yaml.safe_load(f)
            self.local_tree.update(tree)

    def _read_glee_file(self, gleefile_path):

        with open(gleefile_path, 'r') as f:
            fd = yaml.safe_load(f)
        if fd:
            for k, v in fd.items():
                if k and hasattr(self, k):
                    if isinstance(getattr(self, k), str):
                        # if k in['instructor_gitlab_token', 'section_slug', 'default_tz', 'gitlab_course_url']:
                        setattr(self, k, v)
                        continue
                    else:
                        targ = getattr(self, k)
                        targ.update(v)
                else:
                    print('unknown key \'{}\'in .glee file'.format(k))

    def _fix_relative_paths_dict(self, dict):
        for k in dict.keys():
            if not isinstance(dict[k], Path):
                dict.update({k: Path(dict[k])})
            if self.root and self.root not in dict[k].parents:
                if dict[k].is_absolute():
                    dict.update({k: self.root.joinpath(*dict[k].parts[1:])})
                else:
                    dict.update({k: self.root / dict[k]})
        return dict

    def _abs2rel_dict(self, dict, string=False):
        scrap = []
        for k in dict.keys():
            if not isinstance(dict[k], Path):
                p = Path(dict[k])
            else:
                p = dict[k]

            if p.is_absolute():
                if self.root in p.parents:
                    if string:
                        dict.update({k: str(p.relative_to(self.root))})
                    else:
                        dict.update({k: p.relative_to(self.root)})
                else:
                    scrap.append(k)

        for k in scrap:
            del dict[k]

        return dict

    def _connect_to_gitlab(self):
        if not self.gitlab_root:
            _edie('no gitlab root set in ./.glee/glee-vars')
        if self.gitlab is None:
            gl = Gitlab('https://gitlab.com', private_token=self.instructor_gitlab_token)
            gl.auth()
            print('connected to Gitlab.com')
            self.gitlab = gl

    def collect_garbage(self):
        self._collect_garbage()

    @staticmethod
    def find_glee_root(dir):
        p = Path(dir).absolute()

        if str(p) == p.root:
            return None

        if not (p / '.glee').is_dir():
            return Glee.find_glee_root(p.parent)
        else:
            return p

    def save(self, force=False):
        """
        save local tree, gitlab tree, and glee vars
        """

        if not self.__safe_to_save and not force:
            return

        dd = filterPaths(deepcopy(self.as_dict()))

        for key in [
            'root',
            'gitlab',
            'student_info',
            'instructor_gitlab_token',
            'section_slug',
            'gitlab_tree',
            'local_tree',
            '_skeleton_course'
        ]:

            try:
                del dd[key]
            except KeyError:
                pass

        # for key in [
        #     'files',
        # ]:
        #     dd[key] = self._abs2rel_dict(dd[key], string=True)

        if not (self.root / COURSE_CONF_DIR).is_dir():
            os.mkdir(self.root / COURSE_CONF_DIR)

        with open(self.root / COURSE_CONF_GLEE_VARS, 'w') as f:
            yaml.safe_dump(dd, f)

        with open(self.root / COURSE_CONF_GITLAB_TREE, 'w') as f:
            yaml.safe_dump(self.gitlab_tree, f)

        with open(self.root / COURSE_CONF_LOCAL_TREE, 'w') as f:
            yaml.safe_dump(self.local_tree, f)

    def as_dict(self):
        result = {}
        for item in dir(self):
            if item.startswith('_'):
                continue
            if callable(self.__getattribute__(item)):
                continue
            result.update({item: self.__getattribute__(item)})
        return result

    @staticmethod
    def _clone_instructor_repo(source, dest):
        dest = dest.absolute()
        Repo.clone_from(url=str(source), to_path=str(dest))
        try:
            os.remove(dest / COURSE_CONF_SECTION_SLUG)
        except FileNotFoundError:
            pass

    def clone_course(self, source, dest):
        source = Path(source)
        dest = Path(dest)

        try:
            testfile = dest.parent / '.glee_test_file~'
            testfile.write_text('')
            os.remove(testfile)
        except BaseException:
            _edie('can\'t create a project in {}. Check it exists and you have write permission.')

        try:
            self._clone_instructor_repo(source, dest)

        except exc.GitCommandError:
            try:
                source = source / 'instructor.git'
                self._clone_instructor_repo(source, dest)
            except exc.GitCommandError:
                _edie('I couldn\'t find a Glee repo with that URL...')

    def init_repo(self, path):
        targ = Path(path).absolute()

        try:
            Repo(path=str(targ))
        except:
            print('Creating a Git repo')
            # os.makedirs(targ, exist_ok=True)
            shutil.copytree(self.paths['SKELETON_COURSE'], targ)
            rb = Repo.init(path=str(targ))
            (targ / 'README.md').write_text('# Glee Course')
            rb.index.add('README.md')
            rb.index.commit(message='first commit')

        self._load_defaults()
        self.root = targ
        if not USER_CONF_DIR.is_dir():
            os.makedirs(USER_CONF_DIR, exist_ok=True)
        self._load_instructor_vars()

        if self.instructor_gitlab_token:
            print('using Gitlab.com access token from global user conf at {}'.format(self.filenames['USER_CONF_FILE']))

        else:
            token = None
            while not token:
                token = input('What Gitlab API access token would you like to use for this repo? :')
            self.instructor_gitlab_token = token

        section_slug = None
        while not section_slug:
            section_slug = input('What section slug do you want to use (eg \'21sp\')?? : ')
            self.section_slug = section_slug

        for k, d in self.local_tree.items():
            if '_name' in d:
                tdir = Path(d['_name'].replace('::course::', '.'))
                if not Path(self.root / tdir).is_dir():
                    os.makedirs(self.root / tdir, exist_ok=True)

        self.save(force=True)

    def process_schedule(self):
        sch = pd.read_excel(self.files['SCHEDULE_ODS'])
        (Path(self.paths['BULLETIN_PUB']) / 'schedule.md').write_text(sch.to_markdown())
        # cp $(INSTRUCTOR_ROOT)/planning/schedule.csv $(INSTRUCTOR_ROOT)/planning/schedule.md
        # rsync $(INSTRUCTOR_ROOT)/planning/schedule.md $(STUDENT_MATERIAL)/schedule.md

    def process_syllabus(self):
        texf = self.files['SYLLABUS'].with_suffix('.tex')
        pdff = self.files['SYLLABUS'].with_suffix('.pdf')

        subprocess.check_call(['bash', '-c',
                               'cd {}'.format(self.root / self.files['SYLLABUS'].parent)
                               + ' && '
                               + 'pdflatex {}'.format(texf.name)
                               + ' && '
                               + 'pdflatex {}'.format(texf.name)
                               ])

        shutil.copy(self.root / pdff, self.paths['BULLETIN_PUB'] / pdff.name)

        subprocess.check_call(['bash', '-c',
                               'pandoc -s {} -o {}'.format(self.root / texf, self.paths['BULLETIN_PUB'] / 'syllabus.md')
                               ])

    def process_important_links(self):
        with (self.paths['BULLETIN_PUB'] / 'README.md').open('w') as f:
            f.write(Path(self.files['BULLETIN_README_TEMPLATE']).read_text())
            f.write('\n')
            f.write(Path(self.files['IMPORTANT_LINKS']).read_text())

    def process_gradebook(self):
        bulletin_dir = self._resolve_local_path('student-view/BULLETIN')
        target = bulletin_dir / self.filenames['GRADES_FILE_ENCR']
        self.encrypt_grade_markdown(
            input_file=self._resolve_local_path('section-data')/self.filenames['GRADEBOOK'],
            sheet_name=self.ods_sheet_names['gradebook']['GRADE_SHEET_NAME'],
            output_file=target)
        shutil.copy(
            self._skels['show-grades-script'],
            bulletin_dir / self.filenames['SHOW_GRADES_SCRIPT'])
        os.chmod(
            bulletin_dir / self.filenames['SHOW_GRADES_SCRIPT'],
            mode=0o755)

    def _list_posted_assignments(self, ):
        self._connect_to_gitlab()
        assignments_group_path = self._resolve_gl_path('section/student/assignments')
        assignments_group = self.gitlab.groups.get(str(assignments_group_path))
        # assignments_group = [sr
        #                      for sr in self.gitlab.groups.list(search=assignments_group_path, owned=True)
        #                      if sr.full_path == str(assignments_group_path)][0]

        if not assignments_group:
            print('could not find assignments folder: {}'.format(assignments_group_path))
            return None
        else:
            print('found assignments group: {}'.format(assignments_group.web_url))
            return assignments_group.projects.list(all=True) + assignments_group.subgroups.list(all=True)

    def fetch_submission(self, assignment, student='*'):

        posted_assignments = self._list_posted_assignments()

        if not assignment:
            print('available assignments: {}'.format(', '.join(sorted([r.path for r in posted_assignments]))))
            return

        for argassn in assignment:
            for a in posted_assignments:
                if argassn == a.path:

                    self._fork_submission(assignment_slug=argassn, student=student)

                    if isinstance(a, GroupProject):
                        self._fetch_assignment(
                            assmt=self.gitlab.projects.get(a.id),
                            student=student,
                            source='submissions',
                        )
                    elif isinstance(a, GroupSubgroup):
                        self._fetch_assignment(
                            assmt=self.gitlab.projects.get(
                                (self.gitlab.groups.get(a.id).projects.list(search=a.path)[0]).id
                            ),
                            student=student,
                            source='submissions',
                        )

    def _fetch_assignment(self, assmt, student='*', source='submissions'):

        try:
            dd = self.get_due_date(assmt)

        except:
            dd = self._ask_for_duedate()

        si = self.student_info
        if student != '*':
            si = [ss for ss in si for name in student if name.lower() in ss['Gitlab Username'].lower() or name.lower() in ss['Name'].lower()]

        if isinstance(assmt, GroupSubgroup):
            assmt = self.gitlab.groups.get(assmt.id)
        elif isinstance(assmt, GroupProject):
            assmt = self.gitlab.groups.get(assmt.id)

        print('using {}'.format(assmt.web_url))
        print('using due-date: {}'.format(dd))

        MISSING = []
        for stu in si:
            r = self._fetch_student_assignment(stu, assmt, deadline=dd, source=source)
            if isinstance(r, tuple) and r[0] == '[MISSING]':
                MISSING.append((r[1], r[2]))

        if MISSING:
            for sn, sun in MISSING:
                print('[MISSING]: student {} (@{}) has not forked the assignment'.format(sn, sun))

    def _fetch_student_assignment(self, stu, assmt, deadline=None, source='forks'):
        if source == 'submissions':
            # assnmt_grp = self._get_submissions_gl_group().subgroups.list(search=assmt.path)[0]
            assnmt_grp = self.gitlab.groups.get(str(self._resolve_gl_path('section/instructor/submissions/') / assmt.path))
            # assnmt_grp = self.gitlab.groups.get(assnmt_grp.id)
            forks = assnmt_grp.projects.list(all=True)
        elif source == 'forks':
            forks = assmt.forks.list(all=True)
        else:
            raise RuntimeError('\'source\' argument must be either \'submissions\' or \'forks\' ')

        stu_fk = None

        sun = stu['Gitlab Username'].lstrip('@')
        # see if that student has a repo:
        stuser = self.gitlab.users.list(username=sun)[0]

        for fproj in forks:
            if source == 'forks':
                if fproj.creator_id == stuser.id and stuser.web_url in fproj.web_url:
                    stu_fk = fproj
                    break
            elif source == 'submissions':
                if sun in fproj.path:
                    stu_fk = fproj
                    break

        if stu_fk is None:
            # print('[MISSING]: student {} (@{}) has not forked the assignment'.format(stuser.name, stuser.username))
            return ('[MISSING]', stuser.name, stuser.username)

        self._download_for_grading(assmt, stuser, stu_fk, deadline)

    def _download_for_grading(self, assmt, stuser, stu_fk, deadline):

        try:
            student_dir = stuser.name.lower().split(' ')
            student_dir.reverse()
            student_dir = '-'.join(student_dir)

        except:
            student_dir = stuser.name.lower().replace(' ', '-')

        student_path = self._resolve_local_path('grading') / assmt.path / student_dir

        if student_path.is_dir():
            # print('{}\'s assignment dir is already made'.format(stuser.name))
            if (student_path / '.git').is_dir():
                # print('{}\'s assignment dir is a git dir'.format(stuser.name))
                gr = Repo(student_path)
                print('pulling {}\'s latest commits'.format(stuser.name))
                try:
                    gr.remotes['origin'].pull('main')
                except git.exc.GitCommandError:
                    print('could not pull remote/main branch')
                    pass
        else:
            os.makedirs(student_path, exist_ok=False)
            print('cloning {}\'s assignment fork'.format(stuser.name))
            Repo.clone_from(stu_fk.forked_from_project['ssh_url_to_repo'], to_path=student_path)
            gr = Repo(student_path)

        # find last commit before deadline and check it out:
        last_commit = None
        for c in gr.iter_commits():
            cd = c.committed_datetime
            if cd <= deadline:
                last_commit = c
                break

        try:
            if last_commit == gr.heads.main.commit:
                gr.git.checkout('main')
            else:
                new_branch = gr.create_head('deadline')
                new_branch.commit = c
                gr.head.reference = new_branch
        except:
            pass

        # # add student's fork as a remote
        # try:
        #     gr.create_remote(name='student', url=stu_fk.forked_from_project['ssh_url_to_repo'])
        # except exc.GitCommandError as e:
        #     if e.status != 128:
        #         raise e

    # def backup_assignment(self, slug=None):
    #     if slug is None:
    #         _edie('must specify the assignment slug to backup')
    #
    #     src = self.paths['PUBLIC_DIR'] / 'assignments' / slug
    #     dst = self.paths['ASSIGNMENTS'] / slug
    #
    #     assert src.is_dir()
    #
    #     print('going to do:')
    #     subprocess.run(
    #         ['bash',
    #          '-c',
    #          'rsync -auin {}'
    #          ' --exclude "/instructor"'
    #          ' --exclude "/.git*"'
    #          ' --exclude "/gitignore"'
    #          ' {}/ {}/'.format(
    #              '--exclude-from "{}"'.format(src / 'gitignore') if (src / 'gitignore').is_file() else '',
    #              src,
    #              dst)
    #          ],
    #         check=False,
    #     )
    #
    #     if not _confirm():
    #         return
    #
    #     r = subprocess.run(
    #         ['bash',
    #          '-c',
    #          'rsync -aui {}'
    #          ' --exclude "/instructor"'
    #          ' --exclude "/.git*"'
    #          ' --exclude "/gitignore"'
    #          ' {}/ {}/'.format(
    #              '--exclude-from "{}"'.format(src / 'gitignore') if (src / 'gitignore').is_file() else '',
    #              src,
    #              dst)
    #          ],
    #         check=False,
    #     )
    #
    #     if r.returncode:
    #         _edie('rsync unexpectedly quit')
    #
    #     proj_gi = src / 'gitignore'
    #     bk_gi = dst / 'gitignore'
    #     newbkgi = merge_ignores([proj_gi, bk_gi])
    #
    #     with open(bk_gi, 'w') as f:
    #         f.writelines([line + '\n' for line in newbkgi])

    def _read_student_info(self, gradebook_path):
        if isinstance(gradebook_path, str):
            gradebook_path = Path(gradebook_path)

        if not gradebook_path or not gradebook_path.is_file():
            _edie('could not find file \'{}\''.format(self.files['GRADEBOOK']))

        si = pd.read_excel(gradebook_path,
                           sheet_name=self.ods_sheet_names['gradebook']['ROLL_SHEET_NAME'],
                           # index_col='Student Number',
                           # convert_float=True,
                           # converters={'Student Number': int_besteffort},
                           # dtype={'Student Number': int},
                           )

        for idx, row in si.iterrows():
            if not isinstance(row['Index'], int):
                si = si.drop(idx)

        si = si.astype({'Student Number': int, 'Index': int})
        si = si.set_index('Index')

        sd = []
        for idx, row in si.iterrows():
            if not row['Name'] or not row['Status'] == 'Booked':
                continue
            sd.append(row.to_dict())

        # print('using student info:\n{}'.format(si))

        return sd

    def _ask_for_duedate(self, duedate=None):
        if duedate is None or duedate=='':
            dd = _ask_for_duedate()
        else:
            dd = dateparse(duedate)

        if not dd.tzinfo:
            dd = dd.replace(tzinfo=tz.gettz(self.default_tz))
        return dd

    def _get_gitlab_assignment(self, assn_slug):
        self._connect_to_gitlab()
        path = self._resolve_gl_path('section/student/assignments') / assn_slug
        return self.gitlab.projects.get(str(path))

        # assn_group = self._get_assignments_gl_group()
        # proj = None
        # for pp in assn_group.projects.list(all=True,search=assn_slug):
        #     if pp.path == assn_slug:
        #         proj = self.gitlab.projects.get(pp.id)
        #         return proj
        #
        # raise IndexError

        # assn_match_groups = self.gitlab.groups.list(search=str(assn_group_url), order_by='path',
        #                                             include_subgroups=False)
        #
        # try:
        #     assn_group = [item for item in assn_match_groups if item.full_path == str(assn_group_url)][0]
        # except IndexError:
        #     raise RuntimeError('{} does not seem to be a valid Gitlab.com namespace.'.format(assn_group_url))
        #
        # posted_assignments = self.gitlab.groups.get(assn_group.id).projects.list(include_subgroups=True)
        # this_ass_group = [a for a in posted_assignments if a.path == assn_slug][0]
        # proj = self.gitlab.projects.get(this_ass_group.id)
        # return proj

    def _resolve_local_path(self, path):
        realpath = Path(self.root)
        tdict = self.local_tree
        for p in Path(path.lower()).parts:
            realpath /= tdict[p]['_name']
            tdict = tdict[p]

        # realpath = str(realpath).replace('::section::', self.section_slug)
        return Path(realpath)

    def _resolve_gl_path(self, path):
        realpath = Path(self.gitlab_root)
        tdict = self.gitlab_tree
        for p in Path(path).parts:
            realpath /= tdict[p]['_name']
            tdict = tdict[p]

        realpath = str(realpath).replace('::section::', self.section_slug)
        return Path(realpath)

    def _get_assignments_gl_group(self):
        self._connect_to_gitlab()
        assn_group_url = self._resolve_gl_path('section/student/assignments')
        return self._get_gl_group(path=assn_group_url)

    def _get_submissions_gl_group(self):
        self._connect_to_gitlab()
        path = self._resolve_gl_path('section/instructor/submissions')
        return self._get_gl_group(path)

    def _get_gl_user(self, username):
        if not isinstance(username, str):
            username = str(username)
        self._connect_to_gitlab()
        userlist = self.gitlab.users.list(username=username)
        if not userlist:
            _edie('Gitlab could not find a user with username \'{}\''.format(username))
        else:
            return userlist[0]

    def _get_gl_group(self, path):
        if not isinstance(path, str):
            path = str(path)
        self._connect_to_gitlab()
        group = self.gitlab.groups.get(path)
        if not group:
            cands = self.gitlab.groups.list(search=path, include_subgroups=False, order_by='path')
            if cands:
                group = self.gitlab.groups.get(([cand for cand in cands if cand.full_path.lower() == path.lower()][0]).id)
        return group

    def _get_gl_project(self, path):
        if not isinstance(path, str):
            path = str(path)
        if path.startswith('/'):
            path = path[1:]
        self._connect_to_gitlab()
        # cands = self.gitlab.projects.list(search=path, search_namespaces=True, include_subgroups=True, order_by='path')
        return self.gitlab.projects.get(path)

    def make_shared_submission_groups(self):
        parent = self._get_gl_group(self._resolve_gl_path('section/instructor/submissions'))
        access_level = gitlab.const.DEVELOPER_ACCESS

        for stu in self.student_info:
            studentuser = self._get_gl_user(stu['Gitlab Username'])

            try:
                self.gitlab.groups.create({
                    'parent_id': parent.id,
                    'name': stu['Gitlab Username'],
                    'path': stu['Gitlab Username']
                })

            except GitlabCreateError as e:
                if e.response_code == 400:
                    pass
                else:
                    raise e

            rgroup = self._get_gl_group(
                self._resolve_gl_path('section/instructor/submissions') / stu['Gitlab Username'])

            # add student to group membership
            try:
                rgroup.members.create({'user_id': studentuser.id,
                                       'access_level': access_level})

            except GitlabCreateError as e:
                if e.response_code == 409:
                    member = rgroup.members.get(studentuser.id)
                    if member.access_level != access_level:
                        member.access_level = access_level
                        member.save()
                else:
                    raise e

    def make_shared_submission_repos(self, project_name, project_slug):

        for stu in self.student_info:
            # studentuser = self._get_gl_user(stu['Gitlab Username'])
            parent = self._get_gl_group(self._resolve_gl_path('section/instructor/submissions') / stu['Gitlab Username'])

            try:
                self.gitlab.projects.create({
                    'namespace_id': parent.id,
                    'name': project_name,
                    'path': project_slug,
                })
            except GitlabCreateError as e:
                if e.response_code == 400:
                    pass

            proj = self._get_gl_project(self._resolve_gl_path('section/instructor/submissions') / stu['Gitlab Username'] / project_slug)

            try:
                proj.files.get(file_path='README.md', ref='main')

            except GitlabGetError as ex:
                if ex.response_code == 404:
                    proj.files.create({
                        'file_path': 'README.md',
                        'branch': 'main',
                        'content': '\n',
                        'author_email': self.instructor_email,
                        'author_name': self.instructor_name,
                        'commit_message': 'first commit'
                    })
                else:
                    raise ex

    def fetch_student_submission_repos(self, repo_slug):
        for stu in self.student_info:
            self.fetch_student_submission_repo(student=stu, repo_slug=repo_slug)

    def fetch_student_submission_repo(self, student, repo_slug):
        local_root = self._resolve_local_path('grading')
        local_stu_repo_path = local_root / student['Gitlab Username'] / repo_slug
        remote_path = self._resolve_gl_path('section/instructor/submissions') / student['Gitlab Username'] / repo_slug
        remote_repo = self._get_gl_project(remote_path)

        try:
            Repo.clone_from(url=str(remote_repo.ssh_url_to_repo), to_path=str(local_stu_repo_path))
        except git.exc.GitCommandError as e:
            if e.status == 128:
                pass
            else:
                raise e

        local_repo = Repo(local_stu_repo_path)
        local_repo.remotes['origin'].pull('main')

    def fork_submission(self, assignment_slug, student='*'):
        for assn_s in assignment_slug:
            self._fork_submission(assn_s, student=student)

    def _fork_submission(self, assignment_slug, student='*'):
        pub_assn_repo = self._get_gitlab_assignment(assn_slug=assignment_slug)
        submissions_namespace = self._resolve_gl_path('section/instructor/submissions')
        local_forks_namespace = submissions_namespace / assignment_slug

        try:
            local_forks_gl_group = self._get_gl_group(local_forks_namespace)
        except GitlabGetError:
            local_forks_gl_group = None

        if local_forks_gl_group is None:
            submissions_gl_group = self._get_gl_group(submissions_namespace)

            if submissions_gl_group is None:
                section_gl_group = self._resolve_gl_path('section')
                submissions_gl_group = self.gitlab.groups.create({
                    'name': 'submissions',
                    'path': 'submissions',
                    'parent_id': section_gl_group.id
                })

            local_forks_gl_group = self.gitlab.groups.create({
                'name': pub_assn_repo.name,
                'path': pub_assn_repo.path,
                'parent_id': submissions_gl_group.id
            })

        extant_forx = local_forks_gl_group.projects.list(all=True)
        extant_users = [self.gitlab.users.get(self.gitlab.projects.get(ff.forked_from_project['id']).creator_id).username for ff in extant_forx]
        extant_forx = dict(zip(extant_users, extant_forx))

        if student == '*':
            forks = pub_assn_repo.forks.list(all=True)
        else:
            forks = []
            for student_name in student:
                forks += [rp for rp in pub_assn_repo.forks.list(all=True) if student_name.lower() in rp.owner['name'].lower() or student_name.lower() in rp.owner['username'].lower()]

        for ff in forks:
            try:
                if ff.owner['username'] in extant_users:
                    print('{}\'s fork of {} already exists'.format(ff.owner['username'], ff.path))
                    fx_repo = extant_forx[ff.owner['username']]
                    if not (fx_repo.mirror and fx_repo.import_status == 'finished'):
                        my_proj = self.gitlab.projects.get(id=fx_repo.id)
                        my_proj.import_url = 'https://'+self.instructor_gitlab_username+':'+self.instructor_gitlab_token+'@'+ff.http_url_to_repo.lstrip('https://')
                        my_proj.mirror = True
                        my_proj.save()
                        print('enabled pull mirroring for {}'.format(ff.http_url_to_repo.lstrip('https://')))
                    continue

                dst_slug = ff.owner['username']

                # fork it
                this_ass_repo = self.gitlab.projects.get(ff.id).forks.create({
                    'name': ' '.join([pub_assn_repo.name, '--', ff.owner['name']]),
                    'path': dst_slug,
                    'namespace_id': local_forks_gl_group.id,
                })

                this_ass_proj = self.gitlab.projects.get(this_ass_repo.id)
                this_ass_proj.import_url = 'https://' + self.instructor_gitlab_username + ':' + self.instructor_gitlab_token + '@' + ff.http_url_to_repo.lstrip('https://')
                this_ass_proj.mirror = True
                this_ass_proj.save()
                print('forked {}\'s submission into {} (mirroring)'.format(ff.owner['name'], this_ass_repo.web_url))

            except KeyboardInterrupt as e:
                raise e

            except Exception as e:
                print('error creating fork at {}'.format(ff.web_url))

    def post_assignment(self, assn_dir, assn_slug, due_date=None):

        self._connect_to_gitlab()

        src = self._resolve_local_path('assignments') / assn_dir
        dst = self.root / GLEE_TMP / 'assignments' / assn_slug

        if not src.is_dir() or not (src / 'README.md').is_file():
            _edie('I can\'t find a valid assignment at: {}'.format(src))

        dd = self._ask_for_duedate(due_date)

        ga = GleeAssignmentFile(file=src / 'README.md')
        ga.metadata.assign(ga.metadata.date_assigned_field, datetime.datetime.now(tz=tz.tzlocal()).strftime("%c %Z"))
        ga.metadata.assign(ga.metadata.date_due_field, dd.strftime("%c %Z"))
        ga.compute_score(posting=True)

        self._connect_to_gitlab()
        this_ass_group = self._get_assignments_gl_group()

        def check_assn_name(cand_slug):
            try:
                this_ass_repo = self._get_gitlab_assignment(cand_slug)

                if this_ass_repo:
                    def promt():
                        print('About to clobber an existing assignment on Gitlab. Would you like to:\n'
                              '    (m)odify the posted assignment name\n'
                              '    (c)lobber away\n'
                              '    (x) cancel')
                        resp = input().lower()
                        return resp

                    resp = promt()
                    while(not any([resp != chara for chara in ['m','x','c']])):
                        resp = promt()

                    if resp == 'c':
                        # clobber
                        return this_ass_repo

                    if resp == 'm':
                        print('What new name would you like to use for the posted assignment?')
                        newname = input().lower().strip().replace(' ', '-')
                        check_assn_name(newname)

                    if resp == 'x':
                        _edie('Bad assignment name; did not clobber')

            except (IndexError, GitlabGetError):
                return self.gitlab.projects.create({
                    'name': cand_slug + ' - ' + ga.title,
                    'path': cand_slug,
                    'namespace_id': this_ass_group.id,
                })

        this_ass_repo = check_assn_name(assn_slug)

        # Try to clone reference repo:
        try:
            Repo.clone_from(url=this_ass_repo.ssh_url_to_repo, to_path=dst)
        except:
            pass

        #  Do RSYNC
        self._assignment_rsync(src, dst)
        ga.file = dst / 'README.md'
        ga.write()

        try:
            lr = Repo(dst)

        except:
            lr = Repo.init(path=dst)
        lr.index.add('*')
        lr.index.commit('automated commit by Glee (https://gitlab.com/bjmuld/glee)')

        try:
            Remote.add(repo=lr, name='origin', url=this_ass_repo.ssh_url_to_repo)
        except exc.GitCommandError:
            pass

        lr.remote('origin').push(refspec='refs/heads/main:refs/heads/main')
        shutil.rmtree(dst)

        # try:
        #
        #     for stu in self.student_info:
        #
        #         glun = stu['Gitlab Username'].strip().lstrip('@')
        #         short_path = assn_slug + '-' + glun
        #         stu_gl_url = remote_namespace / short_path
        #         name = ga.title + ' - ' + glun
        #         user = self.gitlab.users.list(username=glun)[0]
        #
        #         try:
        #             rr = self.gitlab.projects.get(str(stu_gl_url))
        #
        #         except GitlabGetError:
        #             print('creating repo for {}'.format(glun))
        #             self.gitlab.projects.create({
        #                 'name': name,
        #                 'path': str(short_path),
        #                 'namespace_id': this_ass_group.id,
        #                 'visibility': 'private',
        #             })
        #             rr = self.gitlab.projects.get(str(stu_gl_url))
        #
        #         rr.visibility = 'private'
        #         rr.description = 'Due ' + ga.metadata.get(ga.metadata.date_due_field)
        #         rr.name = name
        #         rr.path = short_path
        #
        #         try:
        #             rr.members.create({'user_id': user.id, 'access_level': DEVELOPER_ACCESS})
        #         except GitlabCreateError as e:
        #             if e.response_code == 409:
        #                 pass
        #             else:
        #                 raise e
        #         try:
        #             rr.protectedbranches.delete('main')
        #         except GitlabDeleteError:
        #             pass
        #         rr.save()
        #
        #         try:
        #             Remote.add(repo=lr, name=glun, url='git@gitlab.com:' + str(stu_gl_url) + '.git')
        #         except exc.GitCommandError:
        #             pass
        #
        #         print('pushing to {}'.format(stu_gl_url))
        #         lr.remote(glun).push(refspec='refs/heads/main:refs/heads/main')
        #
        # finally:
        #     shutil.rmtree(dst)

    def post_solution(self, assn_dir, assn_slug):
        self._connect_to_gitlab()

        src = self._resolve_local_path('assignments') / assn_dir
        tmp = self.root / GLEE_TMP / 'assignments' / assn_slug
        rmt = self._resolve_gl_path('section/student/assignments/')  /assn_slug
        rmt_proj = self._get_gl_project(rmt)

        if not src.is_dir() or not ((src / 'README.md').is_file() or (src / 'student'/ 'README.md').is_file()):
            _edie('I can\'t find a valid assignment at: {}'.format(src))

        if (src / 'solution').is_dir():
            ssrc = src / 'solution'
        elif (src / 'soutions').is_dir():
            ssrc = src / 'soutions'
        else:
            _edie('there is no \'solution\' directory in the local assignment dir.')


        if not rmt_proj:
            _edie('Could not find a published assignment with that slug')

        try:
            tmp_repo = Repo(tmp)
        except:
            try:
                shutil.rmtree(tmp)
            except FileNotFoundError:
                pass
            Repo.clone_from(url=rmt_proj.ssh_url_to_repo, to_path=tmp)
            tmp_repo = Repo(tmp)

        tmp_repo.remotes['origin'].pull('main')

        self._assignment_rsync(ssrc, tmp)
        tmp_repo.index.add('*')
        tmp_repo.index.commit("Solution automatically posted by Glee (https://www.gitlab.com/bjmuld/glee)")
        tmp_repo.remotes['origin'].push()
        shutil.rmtree(tmp)


    @staticmethod
    def _assignment_rsync(src, dst):

        if not Path(dst).parent.is_dir():
            os.makedirs(Path(dst).parent, exist_ok=True)

        cmd = ['rsync',
               '-a',
               '--exclude', '/instructor/',
               '--exclude', '/Instructor/',
               '--exclude', '/INSTRUCTOR/',
               '--exclude', '/solution*/',
               '--exclude', '/Solution*/',
               '--exclude', '/SOLUTION*/',
               '--exclude', '.git/',
               '--exclude', '/.gitignore',
               '--exclude', '/gitignore',
               ]

        if (src / 'gitignore').is_file():
            cmd += ['--exclude-from', str(src / 'gitignore').replace(' ', r'\ ')]

        if (src / '.gitignore').is_file():
            cmd += ['--exclude-from', str(src / '.gitignore').replace(' ', r'\ ')]

        cmd += [(str(src) + '/').replace(' ', r'\ '),
                (str(dst) + '/').replace(' ', r'\ ')]

        r = subprocess.run(cmd, check=False)

        if r.returncode:
            _edie('rsync failed unexpectedly')

        ogi1 = dst / 'gitignore'
        ogi2 = dst / 'gitignore'

        new_gi = merge_ignores([ogi1, ogi2])

        with open(ogi1, 'w') as f:
            f.writelines([item + '\n' for item in new_gi])

        # remove gitignore
        if ogi2.is_file():
            os.remove(ogi2)

    def get_due_date(self, gl_project):
        if isinstance(gl_project, GroupProject):
            gp = self.gitlab.projects.get(gl_project.id)
        elif isinstance(gl_project, GroupSubgroup):
            gp = self.gitlab.groups.get(gl_project.id).projects.list(search=gl_project.path)[0]
            gp = self.gitlab.projects.get(gp.id)
        elif isinstance(gl_project, Project):
            gp = gl_project
        ga = GleeAssignmentFile(text=gp.files.raw('README.md', ref='main').decode())
        dd = dateparse(ga.metadata.data['Date-due'])
        if not dd.tzinfo:
            dd.timetz()
        return dd

    def set_due_date(self, assignment_slug, due_date):

        try:
            due_date = dateparse(due_date)
        except:
            pass

        if not due_date:
            due_date = self._ask_for_duedate()

        if not due_date.tzname():
            due_date = due_date.replace(tzinfo=tz.gettz('America/Chicago'))

        self.post_assignment(assignment_slug, due_date=str(due_date))

    def set_default_tz(self, default_tz=''):
        if default_tz:
            self.default_tz = default_tz
        else:
            while True:
                phrase = input('What timezone would you like to use?: ')
                phrase = phrase.strip()
                if tz.gettz(phrase):
                    self.default_tz = phrase
                    self.save()
                    break
                else:
                    print('Timezone not recognized... Try again.')

    def set_section_slug(self, slug):
        self.section_slug = slug
        self.save()

    def set_gitlab_token(self, token):
        self.instructor['gitlab_token'] = token
        self.save()

        resp = None

        while not resp:
            resp = input('Would you like to set this token for use by other Glee repos (y/n)? ')

        if 'y' in resp.lower():
            cf = USER_CONF_DIR / 'instructor'
            if (cf).is_file():
                with open(cf, 'r') as f:
                    id = yaml.safe_load(f)
            else:
                id = {}
            id.update({'instructor_gitlab_token': token})
            with open(cf, 'w') as f:
                yaml.safe_dump(id, f)

    def set_glee_user_default(self, dictname, key, value):
        if not hasattr(self, dictname):
            setattr(self, dictname, {})
        ddict = getattr(self, dictname)
        ddict.update

    @staticmethod
    def read_assignment_file(file):
        return GleeAssignmentFile(file=file)

    def translate_git_log(self, target_repo):
        git = Repo(target_repo)
        gl_url = git.remotes['origin'].url
        self._connect_to_gitlab()
        stu_repo = self.gitlab.projects.get(gl_url.split(':')[1].split('.git')[0])
        orig_project = self.gitlab.projects.get(id=stu_repo.forked_from_project['id'])
        due_date_text = orig_project.files.raw(file_path=self.filenames['DUEDATE'], ref='main').decode().strip()
        due_date = dateparse(due_date_text)

        r = subprocess.check_output(['git', 'log']).decode()
        translation = []
        for line in r.splitlines():
            o_date_str = re.match(r'^Date:\s*(.*)', line)
            if o_date_str:
                o_date = dateparse(o_date_str.group(1))
                sec_to_duedate = (due_date - o_date).total_seconds()
                if sec_to_duedate < 0:
                    sign = -1
                    sec_to_duedate *= -1
                else:
                    sign = 1

                hrs = sec_to_duedate // (60 * 60)
                sec_to_duedate = sec_to_duedate - (hrs * 60 * 60)
                min = sec_to_duedate // 60
                sec_to_duedate = sec_to_duedate - (min * 60)
                translation.append('Hrs-to-Deadline: {:g}h:{:g}m:{:g}s'.format(sign * hrs, min, sec_to_duedate))
            else:
                translation.append(line)

        print('\n'.join(translation))

    def encrypt_grade_markdown(self, input_file, sheet_name, output_file):
        if hasattr(self, 'student_info'):
            si = self.student_info
        else:
            si = self._read_student_info(input_file)
        # print('using student info:\n{}'.format(si))
        grades = pd.read_excel(
            input_file,
            sheet_name=sheet_name,
            convert_float=True,
            index_col=3)

        md = """
# Course Gradebook
This gradebook is a markdown table in which every entry in every row is obfuscated by that students student-id number
and a password (assigned to you).  To read it, you can run the helper program included in this repo called \"show_grades.py\".
Use the tool in the following way:

```bash
> cd <bulletin>
> ./{} {} <student-number> <password>
```
The `show_grades` tool will decrypt your row in the table and save it as a CSV file which you can open with Excel or LibreOffice

### Grade Table:

| {} |
| {} |
""".format(
        self.filenames['SHOW_GRADES_SCRIPT'],
        self.filenames['GRADES_FILE_ENCR'],
        '|'.join(grades.columns), '|'.join(['------' for i in grades.columns]))

        salt = os.urandom(16)

        def write_row_unencrypted(row, md):
            rowtxt = '|'
            for key, item in row.items():
                if str(item) == 'nan':
                    item = ' '
                if isinstance(item, float):
                    item = '{:0.1f}'.format(item)
                rowtxt += str(item) + '|'
            md += rowtxt + '\n'
            return md

        def write_row_encrypted(row, passw, md):
            kdf = _PBKDF2HMAC(
                algorithm=_hashes.SHA256(),
                length=32,
                salt=salt,
                iterations=100000,
                backend=_default_backend())
            key = base64.urlsafe_b64encode(kdf.derive(passw.encode()))
            cipher_suite = _Fernet(key)
            for key, item in row.items():
                if isinstance(item, float):
                    item = '{:0.1f}'.format(item)
                cipher_text = cipher_suite.encrypt(str(item).encode())
                row[key] = cipher_text.decode()
            return write_row_unencrypted(row, md)

        for idx, row in grades.iterrows():
            if isinstance(row[0], str):
                if row[0].lstrip().startswith('!'):
                    continue
                if row[0].lstrip().startswith('nan'):
                    continue
                elif row[0].lstrip().startswith('#'):
                    md = write_row_unencrypted(row, md)

            elif isinstance(row[0], int):
                try:
                    passw = [stu['password'] for stu in si if stu['Student Number'] == idx][0]
                except IndexError:
                    print('student \'{}\' not included in student data table. Skipping...'.format(row['Name']))
                md = write_row_encrypted(row, passw, md)

            elif isinstance(row[0], float) and np.isnan(row[0]):
                continue
            else:
                raise RuntimeError

        with open(output_file, 'w') as f:
            f.write(md)
            f.write('\n\n' + salt.hex())

    def set_instructor(self, instructor_gitlab_token=None):
        targ = self.files['USER_CONF_FILE']
        if targ.is_file():
            if not _confirm('there already appears to be a Glee user conf file. Update it? (N/y)'):
                return

        if instructor_gitlab_token:
            token = instructor_gitlab_token.strip()
        else:
            token = input('what is the instructor\'s Gitlab.com authorization token?').strip()

        if targ.is_file():
            with open(targ, 'r') as f:
                old_data = yaml.safe_load(f)
        else:
            old_data = {}

        old_data.update({'instructor_gitlab_token': token})

        if not targ.parent.is_dir():
            os.makedirs(targ.parent)

        with open(targ, 'w') as f:
            yaml.safe_dump(old_data, f)

    def create_assignment(self, name='newAssignment', title=None):
        name = _fs_safe(name.strip())
        targ_dir = self._resolve_local_path('assignments') / name

        while targ_dir.is_dir():
            print('there is already an assignment by that name. Try again.')
            name = _fs_safe(input('name : ').strip())
            targ_dir = self._resolve_local_path('assignments') / name

        print('creating new assignment \'{}\' in {}'.format(name, targ_dir.parent))

        longname = input('What (long) title would you like to use for the assignment README.md?  ').strip()

        shutil.copytree(self._skels['assignment'], targ_dir, copy_function=shutil.copy)

        template = jinja2.Template(
            ( targ_dir / 'README.md').read_text())
        text = template.render(title=longname)
        ga = GleeAssignmentFile(text=text)
        ga.file = targ_dir / 'README.md'

        # targ_dir.mkdir()
        ga.write()

        # create 'instructor' subdir:
        #(targ_dir / 'instructor').mkdir()

    def get_gitlab_course_home(self,):
        instructor_repo_url = list(Repo(self.root).remote('origin').urls)[0]
        course_group_name = Path(instructor_repo_url.split(':')[1]).parent
        return course_group_name


def filterPaths(r):
    if isinstance(r, list):
        for i, item in enumerate(r):
            r[i] = filterPaths(r[i])

    if isinstance(r, dict):
        for k, v in r.items():
            r[k] = filterPaths(v)

    if isinstance(r, Path):
        r = str(r)

    return r


def cd_commit_push(directory):
    rp = Repo(directory)
    rp.index.add('*')
    rp.index.commit(message='automated commit by \'Glee\'.')
    rp.remote(name='origin').push()


def merge_ignores(list_of_files):
    lines = set()

    for f in list_of_files:
        if Path(f).is_file():
            lines.update(Path(f).read_text().strip().split())

    return list(lines)


if __name__ == '__main__':
    import sys
    from .cli import _main

    sys.exit(_main())
