#!/usr/bin/env python3

import argparse
import base64
import re

from pathlib import Path
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet
from cryptography.fernet import InvalidToken


def main(file=None, id=None, pw=None, write_csv=True):

    ap = argparse.ArgumentParser()
    ap.add_argument('encrypted_grades', help='relative path to the encrypted markdown file')
    ap.add_argument('student_number', help='your student number')
    ap.add_argument('password', help='your secret password')

    if file and id and pw:
        args = ap.parse_args(args=[str(file), str(id), str(pw)])
    else:
        args = ap.parse_args()

    with open(args.encrypted_grades, 'r') as f:
        md = f.readlines()

    salt = bytes.fromhex(md[-1])
    passw = args.password

    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt,
        iterations=100000,
        backend=default_backend())
    key = base64.urlsafe_b64encode(kdf.derive(passw.encode()))
    cipher_suite = Fernet(key)

    data = []
    for idx, line in enumerate(md):
        if line.lstrip().startswith('|'):
            if all([c in [' ','|','-','_',','] for c in line.strip()]):
                continue
            row = line.lstrip(' |').rstrip('| \n').split('|')
            data += [row]

    ctdata=[]
    for idx, row in enumerate(data):
        if idx == 0:
            ctdata += [row]

        elif idx > 0:
            try:
                index = cipher_suite.decrypt(row[0].encode()).decode()
                if re.match(r'\d+', index):
                    # decrypt row !
                    ctdata += [[cipher_suite.decrypt(item.encode()).decode() for item in row]]
            except InvalidToken:
                if isinstance(row[0], str) and row[0].startswith('#'):
                    ctdata += [row]

    for idx, line in enumerate(ctdata):
        if idx == 0:
            print('|'+'|'.join(['{}'.format(item) for item in line])+'|')
            print('|'+'|'.join(['{}'.format('----') for item in line])+'|')
        else:
            print('|'+'|'.join(['{}'.format(item) for item in line])+'|')

    print('\n')

    if write_csv:
        with open(Path(args.encrypted_grades).parent / (Path(args.encrypted_grades).stem + '_decrypted.csv'), 'w') as f:
            for row in ctdata:
                f.write(','.join([item if ',' not in item else '\"'+item+'\"' for item in row]) + '\n')
            f.write('\n')


if __name__ == '__main__':
    exit(main())
