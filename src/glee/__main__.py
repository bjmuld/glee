import sys
from .cli import _main

if __name__ == '__main__':
    sys.exit(_main())
