# Glee

### Gitlab Educational Environment

---

Glee is a command-line tool for administering educational courses. It is meant to replace the main functionality of BlackBoard, namely:

- Post Announcements (Gitlab's "Issue"/ "Epics")
- Post Assignments   (Individual shared repos)
- Grade Assignments  (a `git clone` and some Glee scripts)
- Posting Grades     (Public Gradebook (markdown) whose rows are individually encrypted; studends decrypt their row with their password)
- more

I am currently using it for several Computer Engineering courses
at the University of Mississippi.

Glee's main components are:

    1. A Gitlab.com namespace for the course
        a. A repo containing all of the instructor's materials
        b. A repo containing reading materials and references
        c. A subgroup for each section/semester
            I. (private) "section-data" repo for storing rolls, gradebook, etc.
            II. (public) student work area (writable by students)
            III. (public) bulletin board (not writable by students)
            IV.  (private) "Assignments" subgroup
                i. "Assignment 1" subgroup
                    A. (private) "student1" repo (accessible only by instructor and "student1")
                    B. (private) "student2" repo (accessible only by instructor and "student2")
                    C. (private) "student3" repo (accessible only by instructor and "student3")
                ii. "Assignment 2" subgroup
                    A. (private) "student1" repo (accessible only by instructor and "student1")
                    B. (private) "student2" repo (accessible only by instructor and "student2")
                    C. (private) "student3" repo (accessible only by instructor and "student3")
                .
                .
                .

    2. A local "Glee" directory (clone of the instructor repo)
        a. Source for basic course collateral:
            I. syllabus source
            II. catalog entry
            III. course description
            IV.  course schedule
        b. A Git submodule of the _active_ section's "section-data" repo
        c. A Git submodule of the "reading materials" repo

### Glee's Commands:

- `init`                         creates a new Glee instructor repo
- `schedule`                     Renders schedule source
- `syllabus`                     Renders syllabus source
- `readme_links`                 Corrects links in bulletin/README.md
- `grades`                       Renders encrypted grade table (markdown)
- `post`                         Posts rendered collateral to Bulletin
- `fetch_assignments`            Clones students' individual repositories for an assignment
- `help`                         Display Glee help
- `test_grades`                  Test decoding of encrypted grade table
- `post_assignment`              Creates all student repos for an assignemnt, adds students as developers
- `set_due_date`                 Modifies due-date for an assignment
- `get_due_date`                 Prints current due-date for an assignment
- `set_section_slug`             Sets the unique "slug" to use for the active section (ie. "21sp" for Spring 2021 session)
- `set_token`                    Sets the instructor's Gitlab access token for using the Gitlab API
- `score`                        Computes the score of an assignment file (add points with `[5]`, take away points with `[-3]`)
- `summarize_scores`             Summarizes classes scores across an assignment
- `git_log`                      Translates the output of `git log` so that date-times are relative to due-date
- `new_assignment`               Creates a new assignement
- `default_timezone`             Sets default TZ for due-date computation
- `clone`                        Clones an existing Glee instructor repo

