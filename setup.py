from setuptools import setup, find_packages
import pathlib
import sys
here = pathlib.Path(__file__).parent.resolve()
sys.path.insert(0, str(here / 'src'))
from glee import __version__ as version


print('added \'{}\' to path'.format(here / 'src'))
print('path: {}'.format(sys.path))


long_description = (here / 'README.md').read_text(encoding='utf-8')

setup(
    name="glee",  # Replace with your own username
    version=version,
    author="Barry Muldrey",
    author_email="barry@muldrey.net",
    description="A convenience package for managing academic courses based in Gitlab",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/bjmuld/glee",
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    include_package_data=True,
    install_requires=['pandas',
                      'python-gitlab',
                      'cryptography',
                      'gitpython',
                      'tabulate',
                      'odfpy',
                      'pyyaml',
                      'jinja2',
                      'yatter@git+https://gitlab.com/bjmuld/yatter.git'],

    entry_points={
        'console_scripts': [
            'glee=glee.cli:_main'
        ]
    },

    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],

    python_requires='>=3.6',

    project_urls={
        'Bug Reports': 'https://gitlab.com/bjmuld/glee/-/issues',
        'Source': 'https://gitlab.com/bjmuld/glee'
    },
)
